<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Pokemons;


class PokemonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        $pokemonList = [
            [
              'name' => 'ivysaur',
              'type' => 'grass | poison',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/2.svg',
            ],
            [
              'name' => 'bulbasaur',
              'type' => 'grass | poison',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
            ],
            [
              'name' => 'wartortle',
              'type' => 'water',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/8.svg',
            ],
            [
              'name' => 'venusaur',
              'type' => 'grass | poison',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/3.svg',
            ],
            [
              'name' => 'charmeleon',
              'type' => 'fire',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/5.svg',
            ],
            [
              'name' => 'charmander',
              'type' => 'fire',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/4.svg',
            ],
            [
              'name' => 'charizard',
              'type' => 'fire | flying',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/6.svg',
            ],
            [
              'name' => 'blastoise',
              'type' => 'water',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/9.svg',
            ],
            [
              'name' => 'caterpie',
              'type' => 'bug',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/10.svg',
            ],
            [
              'name' => 'metapod',
              'type' => 'bug',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/11.svg',
            ],
            [
              'name' => 'metapod',
              'type' => 'bug',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/11.svg',
            ],
            [
              'name' => 'butterfree',
              'type' => 'bug | flying',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/12.svg',
            ],
            [
              'name' => 'squirtle',
              'type' => 'water',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/7.svg',
            ],
            [
              'name' => 'weedle',
              'type' => 'bug | poison',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/13.svg',
            ],
            [
              'name' => 'kakuna',
              'type' => 'bug | poison',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/14.svg',
            ],
            [
              'name' => 'beedrill',
              'type' => 'bug | poison',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/15.svg',
            ],
            [
              'name' => 'pidgeotto',
              'type' => 'normal | flying',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/17.svg',
            ],
            [
              'name' => 'pidgey',
              'type' => 'normal | flying',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/16.svg',
            ],
            [
              'name' => 'pidgeot',
              'type' => 'normal | flying',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/18.svg',
            ],
            [
              'name' => 'rattata',
              'type' => 'normal',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/19.svg',
            ],
            [
              'name' => 'raticate',
              'type' => 'normal',
              'image_url' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/20.svg',
            ],
        ];

        $users = User::all();
       

        foreach ($users as $user) {
            
            $likes = rand(1,2);
            $dislikes = rand(1,2);
            

            for ($like=0; $like <= $likes; $like++) { 
                $wild_pokemon = rand(0, count($pokemonList)-1);
                $catch_pokemon =  $pokemonList[$wild_pokemon];
                $flight = Pokemons::firstOrCreate(
                    [
                        'user_id' => $user['id'],
                        'name' => $catch_pokemon['name']
                    ],
                    [
                        'type' => $catch_pokemon['type'],
                        'image_url' => $catch_pokemon['image_url'],
                        'interest' => '1'
                    ]
                );
            }

            for ($dislike=0; $dislike <= $dislikes; $dislike++) { 
                $wild_pokemon = rand(0, count($pokemonList)-1);
                $catch_pokemon =  $pokemonList[$wild_pokemon];
                $flight = Pokemons::firstOrCreate(
                    [
                        'user_id' => $user['id'],
                        'name' => $catch_pokemon['name']
                    ],
                    [
                        'type' => $catch_pokemon['type'],
                        'image_url' => $catch_pokemon['image_url'],
                        'interest' => '0'
                    ]
                );
            }


           
        }

    
    }
}
