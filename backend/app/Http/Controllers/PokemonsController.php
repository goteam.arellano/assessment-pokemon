<?php

namespace App\Http\Controllers;
use App\Models\User;

use App\Models\Pokemons;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class PokemonsController extends Controller
{
    public function like(Request $request)
    {
        $user_id = auth()->user()->id;
        $instances = Pokemons::where('user_id', $user_id)->where('interest', '1')->count();
       

        if ($instances<3) {
            $pokemon = Pokemons::firstOrCreate(
                [
                    'user_id' => $user_id,
                    'name' => $request['name']
                ],
                [
                    'type' => $request['type'],
                    'image_url' => $request['image_url'],
                    'interest' => '1'
                ]
            );
        }
        return response()->json($this->getInterests($user_id));

    }
    public function dislike(Request $request)
    {
        $user_id = auth()->user()->id;
        $instances = Pokemons::where('user_id', $user_id)->where('interest', '0')->count();
        if ($instances<3) {
            $pokemon = Pokemons::firstOrCreate(
                [
                    'user_id' => $user_id,
                    'name' => $request['name']
                ],
                [
                    'type' => $request['type'],
                    'image_url' => $request['image_url'],
                    'interest' => '0'
                ]
            );
        }
       
        return response()->json($this->getInterests($user_id));


    }

    public function getInterests($user_id)
    {
        $user = User::where('id', $user_id)->with('likes','dislikes')->first();
        return $user;
    }
}
