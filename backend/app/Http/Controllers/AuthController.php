<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = $request->validate([
            "first_name" => 'required|string',
            "last_name" => 'required|string',
            "birthdate" => 'required|string',
            'email' => 'required|string|unique:users,email',
            "password" => 'required|string|confirmed',
        ]); 
        $background_colors = ['D32F2F', 'C2185B', '7B1FA2','512DA8','303F9F','1976D2','00796B','388E3C','388E3C','388E3C','E64A19','455A64'];
        $color_count = count($background_colors)-1;
        $default_image = "https://ui-avatars.com/api/?name=".$fields['first_name']."+".$fields['last_name']."&size=128&background=".$background_colors[rand(0, $color_count)]."&color=fff&format=svg";
        $user = User::create([
            'image_url' => $default_image,
            'first_name' => $fields['first_name'],
            'last_name' => $fields['last_name'],
            'birthdate' => $fields['birthdate'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
        ]);
        $token = $user->createToken('myapptoken')->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];
        return response()->json($response);
    }

    public function login(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]); 
        $user = User::where('email', $fields['email'])->first();
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response(['messsage'=> 'Incorrect Credentials'], 401);
        }
        $token = $user->createToken('myapptoken')->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response()->json($response);

    }

    public function logout(Request $request)
    {

        auth()->user()->tokens()->delete();
        $response = "Logged out";
        return response()->json($response, 201);
    }

    public function search(Request $request)
    {


        $userList = User::where('first_name','like','%'.$request['keyword'].'%')
        ->orWhere('last_name','like','%'.$request['keyword'].'%')
        ->with('likes','dislikes')
        ->limit(10)
        ->get();
        return response()->json($userList);

    }

    public function user(Request $request)
    {
        $user = User::where('id', $request->user()->id)->with('likes','dislikes')->first();
        return response()->json($user);
    }

    public function editProfile(Request $request)
    {
        $fields = $request->validate([
            "first_name" => 'required|string',
            "last_name" => 'required|string',
            "birthdate" => 'required|string',
        ]); 

        $user = User::find(auth()->user()->id);
        $background_colors = ['D32F2F', 'C2185B', '7B1FA2','512DA8','303F9F','1976D2','00796B','388E3C','388E3C','388E3C','E64A19','455A64'];
        $color_count = count($background_colors)-1;
        $default_image = "https://ui-avatars.com/api/?name=".$fields['first_name']."+".$fields['last_name']."&size=128&background=".$background_colors[rand(0, $color_count)]."&color=fff&format=svg";

        $user->first_name = $fields['first_name'];
        $user->last_name = $fields['last_name'];
        $user->birthdate = $fields['birthdate'];
        $user->image_url = $default_image;

        $user->save();
        return response()->json($user);

    }
}
